
function loadFakeDOMforJQuery()
{
var document = self.document = {parentNode: null, nodeType: 9, toString: function() {return "FakeDocument"}};
var window = self.window = self;
var fakeElement = Object.create(document);
fakeElement.nodeType = 1;
fakeElement.toString=function() {return "FakeElement"};
fakeElement.parentNode = fakeElement.firstChild = fakeElement.lastChild = fakeElement;
fakeElement.ownerDocument = document;

document.head = document.body = fakeElement;
document.ownerDocument = document.documentElement = document;
document.getElementById = document.createElement = function() {return fakeElement;};
document.createDocumentFragment = function() {return this;};
document.getElementsByTagName = document.getElementsByClassName = function() {return [fakeElement];};
document.getAttribute = document.setAttribute = document.removeChild = 
document.addEventListener = document.removeEventListener = 
function() {return null;};
document.cloneNode = document.appendChild = function() {return this;};
document.appendChild = function(child) {return child;};
console.log("loaded DOM for JQuery");
}

loadFakeDOMforJQuery();
importScripts("/seedhealthapp_dev/vendor/jquery/jquery-3.2.1.min.js");

//function enterseednumber() {
/*onmessage = function(e) {	

		alert(e.data); 

		$.getJSON("http://52.30.61.201/postgresws/RepetitionCheck.php?id="+ localStorage.getItem("requestId") + "&type=" +localStorage.getItem("MaterialGroup")+ "&essayid=3", function(json) {	
		
		for(var t = 0; t < b+1; t++)
		{
		var seedinputname = 'seednumber'+t;
		if(document.getElementById(seedinputname).value!='')
		{
			
			for (var i = 0; i < temp1.length; i++)
			{				
				var formdata = $(this).serialize() + "&param0=3&param1=" + VTestName + "&param2=" + temp2[i] + "&param3=" + localStorage.getItem("requestId") + 
						 "&param4=21&param7=0&param5=" +localStorage.getItem("MaterialGroup") + "&param6=" + t + "&param9="+d+"&param10=" + document.getElementById(seedinputname).value + "&param11=" + localStorage.getItem("UserID");
				
				$.ajax({
					type: "POST",
					url: "http://52.30.61.201/postgresws/SavePatogens.php",
					data: formdata,
					async: false,
				});
			}
		}
		}

		  });	
		  
		

		//closemodal();*/  
	//} ;  


	 	
self.addEventListener('message', function(e) {
  var data = e.data;
  switch (data.cmd) {
    case 'start':
      //self.postMessage('WORKER STARTED: ' + data.temp2);
	  $.getJSON("http://52.30.61.201/postgresws/RepetitionCheck.php?id="+ data.requestId + "&type=" + data.MaterialGroup + "&essayid=3", function(json) {	
		
		for(var t = 0; t < b+1; t++)
		{
		if(data.boxvaluesp[t]!='')
		{
			
			for (var i = 0; i < temp1.length; i++)
			{				
				var formdata = $(this).serialize() + "&param0=3&param1=" + data.VTestName + "&param2=" + data.temp2[i] + "&param3=" + data.requestId + 
						 "&param4=21&param7=0&param5=" + data.MaterialGroup + "&param6=" + t + "&param9="+data.d+"&param10=" + data.boxvaluesp[t] + "&param11=" + data.UserID;
				
				$.ajax({
					type: "POST",
					url: "http://52.30.61.201/postgresws/SavePatogens.php",
					data: formdata,
					async: false,
				});
			}
		}
		}

		  });
      break;
    case 'stop':
      self.postMessage('WORKER STOPPED: ' + data.msg +
                       '. (buttons will no longer work)');
      self.close(); // Terminates the worker.
      break;
    default:
      self.postMessage('Unknown command: ' + data.msg);
  };
}, false);
